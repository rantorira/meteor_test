/**
 * map core module. helper - if want.
 */


// `autoform` action hooks.
var hooksObject = {
  // When add new object through `ModalForm`,
  // we should bind it to map
  onSuccess: function( formType, result ) {
    // Take object
    var entry = mapObjects.findOne( result );
    // Build marker
    var marker = builMapMarker( entry );
    // Bind marker
    marker.addTo( _Map );
    // Hide modal
    $( '#addNewMapObjectModal' ).modal( 'hide' );
    // let's drink
  },
};
// Bind hook. Cap!
AutoForm.addHooks( 'MapObjectsModalForm', hooksObject, true );


// Get all objects in DB, build markers for these, and group by types
getAllMarkersByType = function () {

  var markers = {};

  // Take all objects
  mapObjects.find( {} ).forEach( function ( entry ) {

    if ( markers[ entry.type ] === undefined ) {
        markers[ entry.type ] = [];
    }
    // Build marker
    var marker = builMapMarker( entry );
    // Group marker
    markers[ entry.type ].push( marker );

  });

  return markers;
};


// Buil marker with popup for DB object
builMapMarker = function ( entry ) {
  // Create icon and bind `className` for this type
  var icon = L.divIcon({
    className: typeToIcons( entry.type ),
  });

  // Build marker with `icon` and set it location `entry.latLon`
  var marker = L.marker( entry.latLon, { icon: icon } );

  // Bind info in popup
  marker.bindPopup(
    Blaze.toHTMLWithData( Template.mapPopupMarkerInfo, { mapObject: entry } )
  ).openPopup();

  return marker;
}
