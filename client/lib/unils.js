// maps icon for object type
typeToIcons = function( entry ) {
  var style = Config.typeToIcons[ entry ] || Config.typeToIcons.default;
  return style + ' ' + Config.iconSizeClass;
}
