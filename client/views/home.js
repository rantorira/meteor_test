/**
 * home.html View settings.
 */


Template.home.helpers({

  reactiveTableSettings: function() {
    return {
      collection: mapObjects,
      rowsPerPage: 10,
      showFilter: true,
      fields: [
        { key: 'type', label: 'Тип' },
        { key: 'name', label: 'Название' },
        { key: 'severityRating', label: 'Уровень опасности' },
        { key: 'description', label: 'Описание' }
      ]
    };
  },

  // Main object form used `autoform quickForm`. Here settings.
  mapObjectFormSettings: function() {
    var mapObjectId = FlowRouter.getParam( 'mapObjectId' );
    var entry = undefined;

    if ( mapObjectId ) {
        entry = mapObjects.findOne({ '_id': mapObjectId });
    }

    // Show edit form
    if ( entry ) {
      return {
        type: 'update',
        buttonContent: 'Сохранить',
        panelName: 'Редактирование: объект : <' + entry.name + '>',
        doc: function() {
          return entry
        },
        showAdditiolanButtons: 1,
      }
    }
    // Show add form otherwise
    else {
      return {
        type: 'insert',
        buttonContent: 'Добавить',
        panelName: 'Новый объект',
        doc: null,
      }
    };
  },

  // Fired when `autoform` renove object.
  // Before we on /edit/<some_id> location.
  onSuccessRemoved: function() {
    return function ( result ) {
      FlowRouter.go( '/' )
    };
  }

});


Template.home.events({

  // If click fired, then show edit form
  'click .reactive-table tbody tr': function ( event ) {
    var entry = this;
    FlowRouter.go( '/edit/' + entry._id );
  },

  // If click `Отмена` on edit form
  'click #rejectUpdating': function ( event ) {
    FlowRouter.go( '/' );
  }

});

