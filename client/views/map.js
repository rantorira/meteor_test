/**
 * map.html View settings.
 */

// Will share map. It need for access outside of template
_Map = {};

Template.map.rendered = function() {

  // Map layer. Cities, roads etc.
  var mainLayer = new L.TileLayer(
    Config.mapMainTileUrl,
    {
      minZoom: 1,
      maxZoom: 18,
      attribution: Config.OSMAttribution
    }
  );

  var allLayers = [ mainLayer ];
  var groupedLayers = {};

  var markers = getAllMarkersByType();

  // Take one type of markers and build layergroup
  Object.keys( markers ).forEach(
    function ( key ) {

      // Build layergroup
      var layer = L.layerGroup( markers[ key ] );

      // Add to group
      groupedLayers[ key ] = layer;

      // Add to all
      allLayers.push( layer );
    }
  );

  // Init map
  _Map = L.map( 'map', {
    center: Config.bishkek,
    zoom: 10,
    layers: allLayers, // bind alllayers
    zoomControl: false,
    doubleClickZoom: false
  });

  // Show modal form and push coordinates there
  function onMapDClick( e ) {
    Session.set( '_doc',
      { latLon: [ e.latlng.lat,  e.latlng.lng ] }
    );

    $( '#addNewMapObjectModal' ).modal( 'show' );
  }

  _Map.on('dblclick', onMapDClick);

  // Build and bind control for groupedLayers
  if( _.size( groupedLayers ) ) {
    L.control.layers( {}, groupedLayers, { position: 'topleft' } ).addTo( _Map );
  }

}


Template.map.helpers({
    mapObjectModalFormSettings: {
      buttonContent: 'Добавить',
      doc: function() {
        var _doc = Session.get( '_doc' );
        
        if ( _doc === undefined ) {
            _doc = {};
        }

        _.extend( _doc, { type: 'Учреждение' } );
        return _doc;
      }
    },
});


Template.mapPopupMarkerInfo.helpers({
  // Need for schema extraction
  mapObjects: function() { return mapObjects },
})

