/**
 * Config for AccountsTemplates.
 * Tune `AccountsTemplates` provided authentication form
 */


// Change email field to username field. Therefore, pop password field,
// remove email field, then add username field.
var pwd_field = AccountsTemplates.removeField( 'password' );
AccountsTemplates.removeField( 'email' );
AccountsTemplates.addFields([
  {
    _id: 'username',
    type: 'text',
    displayName: 'username',
    required: true,
    minLength: 5,
  },
  pwd_field
]);


// Bind params
AccountsTemplates.configure({
    defaultLayout: 'mainLayout',
    defaultLayoutRegions: {},
    defaultContentRegion: 'main',

    // When Logout then go sign-in again. Because only an authorized users is legal
    onLogoutHook: function() {
      FlowRouter.go( '/sign-in' )
    },
    texts: {
      navSignOut: 'Выход', // AccountsTemplates SignOut button
    }
});
