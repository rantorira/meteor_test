/**
 * Contain definition and validation of mapObject collection. 
 */


mapObjects = new Mongo.Collection( 'mapObjects' );

// Schema
mapObjects.attachSchema( new SimpleSchema({

  type: {
    type: String,
    label: 'Тип',
    allowedValues: [
      'Учреждение', 'Транспорт', 'Природа', 'Территория', 'Инфраструктура', 'Человек', 'Неопределённый'
    ],
    defaultValue: 'Учреждение',
  },

  name: {
    type: String,
    label: 'Название',
    // FIO validation
    custom: function() {
      var related = this.field( 'type' );

      if ( related.isSet && related.value === 'Человек' ) {

        // Primitive validation.
        // Cyrillic and Latin alphabet
        var reg = /^[a-z\u0400-\u04FF]+$/ig;
        var fio = this.value.replace(/\s+/g, ' ').trim();

        isInvalid = fio.split( /\s/ ).some( function( element, index ) {
          // Invalid if more than 5 parts or contain non alphabetic symbols
          if ( ! element.match( reg ) || index > 5 ) {
            return 1;
          }
        });

        if( isInvalid ) {
          return 'badNameForHuman';
        }
      }
    }, // function
    max: 100,
  },

  description: {
    type: String,
    label: 'Описание',
    max: 1000,
  },

  latLon: {
    type: [ Number ],
    label: 'Местоположение. [ Широта, Долгота ]',
    decimal: true,
    minCount: 2,
    maxCount: 2,
  },

  severityRating: {
    type: Number,
    label: 'Уровень опасности',
    min: 0,
    max: 10,
    defaultValue: 0,
  }

}));

// Bind message for invalid FIO
SimpleSchema.messages({
  'badNameForHuman': 'Не выглядит как ФИО'
});

