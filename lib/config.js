/**
 * Main config file. For untyped global configs.
 * Provides global `Config` object.
 */

Config = {
  typeToIcons: {
    'Учреждение': 'fa fa-university', 
    'Транспорт': 'fa fa-taxi', 
    'Природа': 'fa fa-tree', 
    'Территория': 'fa fa-flag', 
    'Инфраструктура': 'fa fa-industry', 
    'Человек': 'fa fa-male', 
    'Неопределённый': 'fa fa-thumb-tack',
    'default': 'fa fa-thumb-tack',
  },
  defaultLayer: 'Неопределённый',
  iconSizeClass: 'icon-custom-size',
  mapMainTileUrl: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
  OSMAttribution: 'Map data © OpenStreetMap contributors',
  bishkek: [ 42.85, 74.5 ],
};
