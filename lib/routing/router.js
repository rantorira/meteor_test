/**
 * Routing Cap!
 */

// Any paths should be available only for authorized users
FlowRouter.triggers.enter([ AccountsTemplates.ensureSignedIn ]);


FlowRouter.route( '/', {
  name: 'newObject',
  action: function( params, queryParams ) {
    BlazeLayout.render( 'mainLayout', {
      main: 'home',
      nav: 'nav',
    });
  }
});


FlowRouter.route( '/map', {
  name: 'map',
  action: function( params, queryParams ) {
    BlazeLayout.render( 'mainLayout', {
      main: 'map',
      nav: 'nav',
    });
  }
});


FlowRouter.notFound = {
  action: function() {
    BlazeLayout.render( 'mainLayout', {
      main: '404',
      nav: 'nav',
    });
  }
};


FlowRouter.route( '/edit/:mapObjectId', {
  name: 'editObject',
  action: function( params, queryParams ) {
    BlazeLayout.render( 'mainLayout', {
      main: 'home',
      nav: 'nav',
    });
  }
});
